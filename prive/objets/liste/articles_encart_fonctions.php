<?php

/**
 * Fonctions du squelette associé
 *
 * @plugin     encarts
 * @copyright  2013-2016
 * @author     Cyril
 * @licence    GNU/GPL
 * @package    SPIP\Encarts\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


include_spip('prive/objets/liste/articles_fonctions');
