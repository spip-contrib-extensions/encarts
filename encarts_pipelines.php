<?php

/**
 * Utilisations de pipelines par encarts
 *
 * @plugin     encarts
 * @copyright  2013-2016
 * @author     Cyril
 * @licence    GNU/GPL
 * @package    SPIP\Encarts\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/*
 * Un fichier de pipelines permet de regrouper
 * les fonctions de branchement de votre plugin
 * sur des pipelines existants.
 */

/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 *
 * @param  array $flux Données du pipeline
 *
 * @return array       Données du pipeline
 */
function encarts_affiche_milieu($flux) {
	include_spip('inc/pipelines_ecrire');
	include_spip('inc/utils');
	include_spip('inc/config');

	$e = trouver_objet_exec($flux['args']['exec']);
	if (!$e) {
		return $flux;
	}

	$texte = '';
	$tables_objets = lire_config('encarts/objets');
	$cle_objet = $e['id_table_objet'] ?? '';
	$objet = $e['type'] ?? '';
	$id_objet = (int) ($flux['args'][$cle_objet] ?? 0);

	/**
	 * On traite ici le cas où il n'y a qu'un seul objet sélectionné.
	 * lire_config retournera un string s'il ne rencontre pas un format non sérialisé.
	 * Or, nous avons besoin d'un tableau.
	 */
	if (is_string($tables_objets)) {
		$tables_objets = explode(',', $tables_objets);
	}

	// encarts sur les objets configurés
	if (
		$objet
		&& $id_objet
		&& !$e['edition']
		&& is_array($tables_objets)
		&& in_array(table_objet_sql($objet), $tables_objets)
	) {
		$texte .= recuperer_fond('prive/objets/editer/liens', [
			'table_source' => 'encarts',
			'objet' => $objet,
			'id_objet' => $id_objet,
		]);
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Optimiser la base de données en supprimant les liens orphelins
 * de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 *
 * @pipeline optimiser_base_disparus
 *
 * @param  array $flux Données du pipeline
 *
 * @return array       Données du pipeline
 */
function encarts_optimiser_base_disparus($flux) {
	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(['encart' => '*'], '*');

	return $flux;
}

/**
 * Traiter les textes contenant des <encart> .... </encart>
 * ou des <marge>...</marge>
 * en les remplaçant par un span...
 *
 * @param string $texte à analyser
 * @return string texte modifié
 **/
function encarts_pre_propre($texte) {
	if (false !== strpos($texte, '<')) {
		if (preg_match_all(',<(' . _TYPES_ENCARTS . ')>(.*?)</\1>,is', $texte, $regs, PREG_SET_ORDER)) {
			foreach ($regs as $reg) {
				$css = 'encart';
				if ($reg[1] != 'encart') {
					$css .= ' ' . $reg[1];
				}
				$texte = str_replace($reg[0], "<span class='$css'>" . $reg[2] . '</span>', $texte);
			}
		}
	}

	return $texte;
}

/**
 * Mettre les vu=oui lorsque l'on met un modèle
 * d'encart dans un texte.
 *
 **/
function encarts_post_edition($flux) {
	include_spip('inc/utils');
	include_spip('base/objets');

	if (
		($flux['args']['action'] ?? '') === 'modifier'
		&& !in_array($flux['args']['table'] ?? '', ['spip_forum', 'spip_signatures'])
	) {
		$serveur = $flux['args']['serveur'] ?? '';
		$table_sql = $flux['args']['table'];
		$table = table_objet($flux['args']['table']);
		$objet = objet_type($table_sql, $serveur);
		$primary = id_table_objet($table_sql, $serveur);
		$id_objet = $flux['args']['id_objet'];

		$marquer_doublons_encart = charger_fonction('marquer_doublons_encart', 'inc');
		$marquer_doublons_encart($flux['data'], $id_objet, $objet, $primary, $table, $table_sql, [], $serveur);
	}

	return $flux;
}
