<?php

/*
 * (issu du plugin mediatheque)
 * (c) 2009 cedric
 * Distribue sous licence GPL
 * modifie par marcimat.
 */

// On liste tous les champs susceptibles de contenir des encarts si on veut que ces derniers soient lies a l objet lorsqu on y fait reference par encartXX
// la dist ne regarde que chapo et texte, on laisse comme ca, mais ca permet d etendre a descriptif ou toto depuis d autre plugin comme agenda ou grappe
$GLOBALS['encarts_liste_champs'][] = 'texte';
$GLOBALS['encarts_liste_champs'][] = 'chapo';


/**
 * @param array $champs
 * @param int $id_objet
 * @param string $objet
 * @param string $primary
 * @param string $table_objet
 * @param string $table_objet_sql
 * @param array $desc
 * @param string $serveur
 * @return void
 */
function inc_marquer_doublons_encart_dist($champs, $id_objet, $objet, $primary, $table_objet, $table_objet_sql, $desc = [], $serveur = '') {
	include_spip('inc/utils');
	include_spip('inc/lien');
	include_spip('base/abstract_sql');
	$champs_selection = [];
	$champs_a_traiter = '';
	foreach ($GLOBALS['encarts_liste_champs'] as $champs_choisis) {
		if (isset($champs[$champs_choisis])) {
			array_push($champs_selection, $champs_choisis);
		}
	}
	if (count($champs_selection) == 0) {
		return;
	}
	if (empty($desc)) {
		$trouver_table = charger_fonction('trouver_table', 'base');
		$desc = $trouver_table($table_objet, $serveur);
	}
	$load = '';
	// charger le champ manquant en cas de modif partielle de l'objet
	// seulement si le champ existe dans la table demandée

	foreach ($champs_selection as $champs_a_parcourir) {
		if (isset($desc['field'][$champs_a_parcourir])) {
			$load = $champs_a_parcourir;
			$champs_a_traiter .= $champs[$champs_a_parcourir];
		}
	}

	if ($load) {
		$champs[$load] = '';
		$row = sql_fetsel($load, $table_objet_sql, "$primary=" . sql_quote($id_objet));
		if (isset($row[$load])) {
			$champs[$load] = $row[$load];
		}
	}
	$GLOBALS['doublons_encarts_inclus'] = [];
	traiter_modeles($champs_a_traiter, ['encarts' => ['encart']]); // detecter les doublons
	sql_updateq('spip_encarts_liens', ['vu' => 'non'], 'id_objet=' . intval($id_objet) . ' AND objet=' . sql_quote($objet));
	if (count($GLOBALS['doublons_encarts_inclus'])) {
		// on repasse par une requete sur spip_encarts pour verifier que les encarts existent bien !
		$in_liste = sql_in(
			'id_encart',
			$GLOBALS['doublons_encarts_inclus']
		);
		$res = sql_select('id_encart', 'spip_encarts', $in_liste);
		while ($row = sql_fetch($res)) {
			// Creer le lien s'il n'existe pas deja
			sql_insertq('spip_encarts_liens', [
				'id_objet' => $id_objet,
				'objet' => $objet,
				'id_encart' => $row['id_encart'],
				'vu' => 'oui'
			]);
			sql_updateq('spip_encarts_liens', ['vu' => 'oui'], 'id_objet=' . intval($id_objet) . ' AND objet=' . sql_quote($objet) . ' AND id_encart=' . $row['id_encart']);
		}
	}
}
