<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_encart' => 'Ajouter cet encart',

	// B
	'bouton_associer' => 'Ajouter cet encart',
	'bouton_dissocier' => 'Détacher',

	// I
	'icone_creer_encart' => 'Créer un encart',
	'icone_modifier_encart' => 'Modifier cet encart',
	'info_1_encart' => 'Un encart',
	'info_1_utilisation' => '1 utilisation',
	'info_actions'=> 'Actions',
	'info_aucun_encart' => 'Aucun encart',
	'info_encarts_auteur' => 'Les encarts de cet auteur',
	'info_nb_encarts' => '@nb@ encarts',
	'info_nb_utilisations' => '@nb@ utilisations',

	// L
	'label_texte' => 'Texte',
	'label_titre' => 'Titre',

	// R
	'retirer_lien_encart' => 'Retirer cet encart',
	'retirer_tous_liens_encarts' => 'Retirer tous les encarts',

	// S
	'supprimer_encart' => 'Supprimer cet encart',
	'supprimer_encart_confirmer' => 'Êtes-vous sûr de vouloir supprimer cet encart&nbsp;? Cette action est définitive.',
	'supprimer_encart_explication' => 'La suppression de cet encart risque de rompre des liens vers cet encart.',

	// T
	'texte_ajouter_encart' => 'Ajouter un encart',
	'texte_changer_statut_encart' => 'Ce encart est :',
	'texte_creer_associer_encart' => 'Créer et associer un encart',
	'titre_encart' => 'Encart',
	'titre_encarts' => 'Encarts',
	'titre_encarts_rubrique' => 'Encarts de la rubrique',
	'titre_langue_encart' => 'Langue de cet encart',
	'titre_logo_encart' => 'Logo de cet encart',
	'titre_objets_lies_encart' => "Liés à cet encart :",
);

