<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'encarts_titre' => 'encarts',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'champ_objets_label' => 'Objets',
	'champ_objets_explication' => 'Choisissez les objets sur lesquels activer les encarts',

	// T
	'titre_page_configurer_encarts' => 'Paramétrage des encarts',
);

